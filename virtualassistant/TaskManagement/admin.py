# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from TaskManagement.models import TaskCategory
from django.contrib import admin

# Register your models here.
class TaskCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'category_type')
    list_filter = ('category_type',)


admin.site.register(TaskCategory, TaskCategoryAdmin)