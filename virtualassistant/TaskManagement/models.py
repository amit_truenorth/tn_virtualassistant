# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class TaskCategory(models.Model):
    name = models.CharField(max_length=50)
    category_type = models.CharField(max_length=20, default='NM')
    category_name = models.CharField(max_length=100, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return '{}-{}'.format(self.name, self.category_type)
