import os


PROJECT_APP_PATH = os.path.dirname(os.path.abspath(__file__))

BROKER_URL = 'amqp://guest@localhost//'
#CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']  # Ignore other content
CELERY_RESULT_SERIALIZER = 'json'

CELERY_TASK_RESULT_EXPIRES = 0  # Never expire task results
CELERY_IGNORE_RESULT = True

CELERY_TRACK_STARTED = True
CELERY_IMPORTS = (	
	'TaskManagement.tasks'
)

f = os.path.join(PROJECT_APP_PATH, "local_celeryconfig.py")
if os.path.exists(f):
	exec(open(f, "rb").read())