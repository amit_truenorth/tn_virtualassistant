from django.conf.urls import url, include
from django.contrib import admin
from .views import *
from django.conf import settings
from django.conf.urls.static import static
from CalendarManagement import urls as cm_urls
from TaskManagement import urls as tm_urls


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', login_template),
    url(r'^login/view/$', login_view),
    url(r'^logout/$', logout_view),
    url(r'^a/$', index),
    url(r'^partials_templates/homepage/$', homepage),
    url(r'^tm/', include(tm_urls)),
    url(r'^cm/', include(cm_urls)),
]+ static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)