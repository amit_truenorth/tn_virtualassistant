from django.contrib.auth import login, authenticate, logout
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.shortcuts import render_to_response
import json


def logout_view(request):
    logout(request)
    return JsonResponse({
        "status": True,
        "message": "Logout Successfully",
        "redirect_url": '/'
    })


def login_template(request):
    return render_to_response('login.html')


def index(request):
    return render_to_response('index.html')


def homepage(request):
    return render_to_response('partials_templates/homepage.html')

@csrf_exempt
def login_view(request):
    params = json.loads(request.body)
    username = params.get('username')
    password = params.get('password')
    if not password:
        return JsonResponse({
            "message": "Please enter password",
            "status": False
        })

    user = authenticate(username=username, password=password)

    if not user or not user.is_active:
        return JsonResponse({
            "message": "Invalid Login",
            "status": False
        })

    login(request, user)

    return JsonResponse({
        "redirect_url": '/a/',
        "status": True,
        "message": "Login successful"
    })
