var virtualassistantControllers = angular.module('virtualassistantControllers', ['ui-notification']);
virtualassistantControllers.config(['$routeProvider', 'NotificationProvider', '$httpProvider', function($routeProvider, NotificationProvider, $httpProvider) {

  NotificationProvider.setOptions({
    delay: 3000,
    startTop: 20,
    startRight: 10,
    verticalSpacing: 20,
    horizontalSpacing: 20,
    positionX: 'right',
    positionY: 'top'
  });
}]);

virtualassistantControllers.controller('HomepageCtrl', ['$scope', '$log', '$http', '$timeout', '$routeParams', function($scope, $log, $http, $timeout, $routeParams, Notification) {
  $log.info('virtualassistantControllers controller loads');
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  $scope.toggle = true;

  $scope.change_toggle = function(value) {
    $scope.toggle = value;
  }

  $scope.logout = function() {
    $http.get('/logout/').
    success(function(data, status, headers, config) {
      if (data.status) {
        window.location = data.redirect_url;
      } else {
        Notification.error(data.message)
      }
    }).
    error(function(data, status, headers, config) {
      console.log(data);
    });
  }

  $('.button-collapse').sideNav();

  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultDate: '2014-06-12',
    defaultView: 'month',
    editable: true,
    events: [{
        title: 'All Day Event',
        start: '2014-06-01'
      },
      {
        title: 'Long Event',
        start: '2014-06-07',
        end: '2014-06-10'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2014-06-09T16:00:00'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2014-06-16T16:00:00'
      },
      {
        title: 'Meeting',
        start: '2014-06-12T10:30:00',
        end: '2014-06-12T12:30:00'
      },
      {
        title: 'Lunch',
        start: '2014-06-12T12:00:00'
      },
      {
        title: 'Birthday Party',
        start: '2014-06-13T07:00:00'
      },
      {
        title: 'Click for Google',
        url: 'http://google.com/',
        start: '2014-06-28'
      }
    ]
  });

}]);