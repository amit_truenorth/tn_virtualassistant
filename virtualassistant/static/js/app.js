var eLabApp = angular.module('eLabApp', [
	'ngRoute',
	'virtualassistantControllers',
	'ui-notification'
	]);

eLabApp.config(['$routeProvider', 'NotificationProvider', function($routeProvider, NotificationProvider){
	NotificationProvider.setOptions({
		delay: 3000,
		startTop: 20,
		startRight: 10,
		verticalSpacing: 20,
		horizontalSpacing: 20,
		positionX: 'right',
		positionY: 'top'
	});


	$routeProvider.
	when('/homepage/', {
		templateUrl: '/partials_templates/homepage/',
		controller: 'HomepageCtrl'
	}).
	otherwise({
		redirectTo: '/homepage/'
	});
}]);
