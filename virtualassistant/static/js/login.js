var loginApp = angular.module('loginApp', [
        'ui-notification'
    ]);
loginApp.config(['NotificationProvider', '$httpProvider', function(NotificationProvider, $httpProvider){
    NotificationProvider.setOptions({
        delay: 3000,
        startTop: 20,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'top'
    });
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);
loginApp.controller('loginAppCtrl', function($scope, $log, $http, Notification){
	$log.info('Login controller loads');
	$scope.username = '';
	$scope.password = '';

	$scope.login = function(){
		$http.post('/login/view/', {username:$scope.username, password:$scope.password}).
        success(function(data, status, headers, config) {
            console.log(data);
            if (data.status){
                window.location = data.redirect_url;
            }else{
                Notification.error(data.message)
            }
        }).
        error(function(data, status, headers, config) {
            console.log(data);
        });
	}
});
