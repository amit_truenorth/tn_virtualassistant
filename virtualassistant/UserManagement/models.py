# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models

from VAUtils.models import AuditColumns
from virtualassistant.settings import PERSON_PICTURE_UPLOAD_PATH


class PersonMaster(AuditColumns):
	authUser = models.OneToOneField(User)
	firstName = models.CharField(max_length=100)
	lastName = models.CharField(max_length=100)
	middleName = models.CharField(max_length=100, null=True, blank=True)
	gender = models.CharField(max_length=2, choices=( ('M', 'Male'), ('F', 'Female')))
	picture = models.ImageField(null=True, blank=True, upload_to=PERSON_PICTURE_UPLOAD_PATH)

class GroupMaster(AuditColumns):
    groupName = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    groupLabel = models.CharField(max_length=100, null=True, blank=True)
    tag = models.TextField(null=True, blank=True)

class UserGroupRelation(AuditColumns):
	user=models.ForeignKey(PersonMaster)
	group=models.ForeignKey(GroupMaster)
	role=models.CharField(max_length=3,choices=(('OW','Owner'),
											   ('MB','Member'),
											   ('GM','Manager')))


