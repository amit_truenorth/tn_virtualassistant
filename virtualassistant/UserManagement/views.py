# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from UserManagement.models import PersonMaster

# Create your views here.

def create_person(firstName,lastName,middleName=None,userObj=None):
	person_obj=PersonMaster(firstName=firstName,
		middleName=middleName,
		lastName=lastName,
		createdBy=userObj,
		modifiedBy=userObj,
		recordValidFlag='Y')
	person_obj.save()
	

