# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from CalendarManagement.models import Master

# Register your models here.
class MasterAdmin(admin.ModelAdmin):
    list_display = ('name', 'email','json_file_name')
    

admin.site.register(Master, MasterAdmin)