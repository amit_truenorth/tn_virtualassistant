# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
import datetime,dateutil.parser
from datetime import timedelta
from CalendarManagement.models import Master
from CalendarManagement.events import getEvents,create_event
from CalendarManagement.slots import get_slots,get_full_day_slot
from VAUtils.dataconversions import convert_date_to_datetime_min,convert_date_to_datetime_max
from VAUtils.dataconversions import combine_date_time_to_datetime,add_duration_to_datetime
from virtualassistant.settings import TRUENORTH_TIMEZONE
# Create your views here.

@csrf_exempt
def eventList(request):
	post=request.GET	
	sDate=post.get('startdate') # 'YYYY-MM-DD'
	eDate=post.get('enddate') #'YYYY-MM-DD'	
	location=post.get('location')
	modeInterview=post.get('interviewType')
	emailList=post.getlist('emailId')
	empty_slots=[]
	empty_dict={}
	output_list=[]
	eventday_list=[]

	try:

		for emailId in emailList: 			
			eventDict=None

			try:
				eventDict=getEvents(emailId,
					convert_date_to_datetime_min(sDate),
					convert_date_to_datetime_max(eDate),
					)
			except Exception as e:				
				return HttpResponse(str(e),status=400)

			userdict={}
			uname=emailId
			appt_list=[]
			prev_day=[]		

			if eventDict:
				for event in eventDict:				
					start = event['start'].get('dateTime', event['start'].get('date')) # get start of the day
					end=event['end'].get('dateTime', event['end'].get('date')) # get the end of the day
					day=dateutil.parser.parse(start).strftime('%Y-%m-%d')
					eventday_list.append(day)

					if prev_day!=day:   # check if the day is changed
						c=get_slots(appt_list,prev_day)					
						if c:
							empty_slots.append(c)
						
						appt_list=[]						
					
					day_list=()  # the day list is empty at first
					
					day_list=(datetime.datetime.strptime(dateutil.parser.parse(start).strftime('%Y-%m-%dT%H:%M'),'%Y-%m-%dT%H:%M'),
						datetime.datetime.strptime(dateutil.parser.parse(end).strftime('%Y-%m-%dT%H:%M'),'%Y-%m-%dT%H:%M'))

					appt_list.append(day_list)
					prev_day=day	

				empty_slots.append(get_slots(appt_list,prev_day))
				#extra_days_slots=get_extra_days_free_slots(sDate,eDate,eventday_list)
				#empty_slots.append(get_extra_days_free_slots(sDate,eDate,eventday_list))
				d=get_extra_days_free_slots(sDate,eDate,eventday_list)
				if d:
					for it in d:
						empty_slots.append(it)
					
				userdict[uname]=empty_slots
				output_list.append(userdict)
				empty_slots=[]
			else:				
				userdict[uname]=get_single_slots(sDate,eDate)
				output_list.append(userdict)

		empty_dict['freeslots']=output_list
		return JsonResponse(empty_dict)
	except Exception as e:	
		print 'here'
		print str(e)	
		return HttpResponse('Error in calculating free slots',status=400)


@csrf_exempt
def insert_event(request):
	post=request.GET 
	emailId=post.get('emailId')
	attendeeId=post.get('attendeeId')
	duration=int(post.get('duration')) #min
	startDate=post.get('startDate')
	startTime=post.get('startTime')
	location=post.get('location')
	modeInterview=post.get('interviewType')
	summary=post.get('summary')

	try:
		start=add_duration_to_datetime(startDate,startTime,-330, 'MIN')
		end=add_duration_to_datetime(startDate,startTime,-330+duration,'MIN')
		create_event(emailId,
			        start,
			        end,
			        attendeeId,
			        summary=summary)
		return HttpResponse('Event Created',status=200)
	except Exception as e:		
		return HttpResponse(str(e),status=400)

def get_single_slots(sDate,eDate):
	output_list=[]
	startDate=datetime.datetime.combine(datetime.datetime.strptime(sDate,'%Y-%m-%d'),datetime.time.min)
	endDate=datetime.datetime.combine(datetime.datetime.strptime(eDate,'%Y-%m-%d'),datetime.time.max)

	delta=endDate-startDate

	for i in range(delta.days + 1):
		date=(startDate+timedelta(days=i)).strftime('%Y-%m-%d')
		c=get_full_day_slot(date)

		if c:
			output_list.append(c)
	return output_list
		
    

def get_extra_days_free_slots(sDate,eDate,eventday_list):

	
	extra_days_list=[]
	total_days_list=[]
	
	startDate=datetime.datetime.combine(datetime.datetime.strptime(sDate,'%Y-%m-%d'),datetime.time.min)
	endDate=datetime.datetime.combine(datetime.datetime.strptime(eDate,'%Y-%m-%d'),datetime.time.max)

	delta=endDate-startDate
	for i in range(delta.days + 1):
		total_days_list.append((startDate+timedelta(days=i)).strftime('%Y-%m-%d'))
	
	diff_list=set(total_days_list)-set(eventday_list)
	for items in diff_list:
		c=get_full_day_slot(items)
		if c:
			extra_days_list.append(c)
	
	return extra_days_list

@csrf_exempt
def get_days_events(request):
	post=request.GET	
	sDate=post.get('caldate') # 'YYYY-MM-DD'
	emailId=post.get('emailId')

	output_list=[]
	output_dict={}
	try:
		eventDict=getEvents(emailId,
					convert_date_to_datetime_min(sDate),
					convert_date_to_datetime_max(sDate),
					)
		for event in eventDict:
			output_list.append ({'start':event['start'].get('dateTime', event['start'].get('date')),
								'summary':event['summary'],})			

		output_dict['events']=output_list	
		return JsonResponse(output_dict)
	except Exception as e:
		return HttpResponse(str(e),status=400)


@csrf_exempt
def add_events(request):
	post=request.GET 
	emailId=post.get('emailId')	
	duration=int(post.get('duration')) #min
	startDate=post.get('startDate')
	startTime=post.get('startTime')
	summary=post.get('summary')

	try:
		start=add_duration_to_datetime(startDate,startTime,-330, 'MIN')
		end=add_duration_to_datetime(startDate,startTime,-330+duration,'MIN')
		create_event(emailId,
			        start,
			        end,
			        None,
			        summary)
		return HttpResponse('Event Created',status=200)
	except Exception as e:		
		print str(e)
		return HttpResponse('Error in Processing',status=400)