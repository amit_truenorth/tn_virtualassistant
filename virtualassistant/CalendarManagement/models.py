# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from VAUtils.models import AuditColumns
# Create your models here.

class Master(models.Model):
    name = models.CharField(max_length=50)
    email=models.EmailField(null=True, blank=True)
    json_file_name=models.CharField(max_length=100,blank=True,null=True)

    def __unicode__(self):
        return '{}-{}'.format(self.email, self.json_file_name)