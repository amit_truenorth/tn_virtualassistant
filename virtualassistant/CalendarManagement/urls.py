from django.conf.urls import url
from CalendarManagement import views as cmviews
urlpatterns = [

	#url(r'mailDailyData/$', views.daily_data_temp),
    url(r'getFreeSlots/?$', cmviews.eventList),  
    url(r'addEvent/?$', cmviews.add_events),   
    url(r'scheduleInterview/?$', cmviews.insert_event), 
    url(r'getEvents/?$', cmviews.get_days_events), 


]