from datetime import datetime, timedelta,time


def get_slots(appt_list,day):
	empty_list=[]	
	if day:		
		start_list=[]
		end_list=[]

		duration=timedelta(hours=1)	
		stime=datetime.strptime('09:30 AM','%I:%M %p').time()
		etime=datetime.strptime('06:00 PM','%I:%M %p').time()
		start_time=datetime.combine(datetime.strptime(day,'%Y-%m-%d'),stime)
		end_time=datetime.combine(datetime.strptime(day,'%Y-%m-%d'),etime)

		hours=(start_time,end_time)

		if appt_list:
			
			slots = sorted([(hours[0], hours[0])] + appt_list + [(hours[1], hours[1])])		
			for start, end in ((slots[i][1], slots[i+1][0]) for i in range(len(slots)-1)):
				print start,end
				#assert start <= end, "Cannot attend all appointments"
				
				while start + duration <= end:					
					timeslot=(start,start+duration,'N')					
					empty_list.append(timeslot)
					start += duration
		else:
			empty_list=(start_time,end_time,'N')
		
	return empty_list


def get_full_day_slot(day):
	empty_list=[]

	if check_weekday(day):		
		sttime=datetime.strptime('09:30 AM','%I:%M %p').time()
		ettime=datetime.strptime('06:00 PM','%I:%M %p').time()
		start_time=datetime.combine(datetime.strptime(day,'%Y-%m-%d'),sttime)
		end_time=datetime.combine(datetime.strptime(day,'%Y-%m-%d'),ettime)	
		hours=(start_time,end_time,'N')
		empty_list.append(hours)

		
	return empty_list

def check_weekday(day):
	
	local_dt=datetime.combine(datetime.strptime(day,'%Y-%m-%d'),time.min)
	
	if local_dt.weekday()> 4:
		return False
	else:
		return True
