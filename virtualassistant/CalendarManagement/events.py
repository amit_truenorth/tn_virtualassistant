
from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from CalendarManagement.models import Master
from CalendarManagement.auth import build_service
import datetime

try:
    import argparse
    # flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    flags = tools.argparser.parse_args([])
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'


def get_credentials(filename):

    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')
    if filename:
        credential_path=os.path.join(credential_dir,filename)
    store = Storage(credential_path)
    credentials = store.get()
    
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def getEvents(emailId,startDate=None,endDate=None):
    """Shows basic usage of the Google Calendar API.

    Creates a Google Calendar API service object and outputs a list of the next
    10 events on the user's calendar.
    """

    #for items in ['calendar-python-quickstart1.json','calendar-python-quickstart.json']:
    # credentials = get_credentials(fileName)
    # http = credentials.authorize(httplib2.Http())
    # service = discovery.build('calendar', 'v3', http=http)

    #now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time

    service=build_service()

    eventsResult = service.events().list(
        calendarId=emailId, singleEvents=True, timeMin=startDate,timeMax=endDate,        
        orderBy='startTime').execute()
    events = eventsResult.get('items', [])
    return events


def create_event(emailId,startTime,endTime,attendeeId=None,summary=None):
    
    service=build_service()
    emailList=[]

    if summary is None:
        summary='Interview for position of Developer'

    event={
        'summary':summary,
        'location':'Mumbai',
        'start':{
            'dateTime':startTime,           
        },
        'end':{
            'dateTime':endTime,
            #'timeZone':'Asia/Calcutta',
        },              
    }

    if attendeeId:
        emailList.append({'email':attendeeId})
        event['attendees']=emailList        
    
    event = service.events().insert(calendarId=emailId, body=event).execute()


