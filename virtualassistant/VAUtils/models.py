# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


class AuditColumns(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    createdBy = models.ForeignKey(User, related_name='+')
    modified = models.DateTimeField(auto_now=True)
    modifiedBy = models.ForeignKey(User, related_name='+')
    recordValidFlag = models.CharField(max_length=1, default='Y')

    class Meta:
        abstract = True
