import datetime,pytz
from virtualassistant.settings import TRUENORTH_TIMEZONE

def convert_date_to_datetime_min(idate):
	
	local_dt=datetime.datetime.combine(datetime.datetime.strptime(idate,'%Y-%m-%d'),datetime.time.min)
	#local_dt=local_dt.replace(tzinfo=pytz.UTC)	
	#local_dt=tz.localize(local_dt, is_dst=True)
	#local_dt=local_dt.astimezone(TRUENORTH_TIMEZONE)
	return local_dt.isoformat() + 'Z'

def convert_date_to_datetime_max(idate):
	
	local_dt=datetime.datetime.combine(datetime.datetime.strptime(idate,'%Y-%m-%d'),datetime.time.max)
	#local_dt=local_dt.replace(tzinfo=pytz.UTC)	
	#local_dt=tz.localize(local_dt, is_dst=True)
	#local_dt=local_dt.astimezone(TRUENORTH_TIMEZONE)
	return local_dt.isoformat() + 'Z'
	
def combine_date_time_to_datetime(idate,itime):
	#idate format 'YYYY-MM-DD'
	#iTime format '24Hr Format'
	sdate=datetime.datetime.strptime(idate,'%Y-%m-%d')
	stime=datetime.datetime.strptime(itime,'%I:%M %p').time()
	local_dt=datetime.datetime.combine(sdate,stime)
	#local_dt=tz.localize(local_dt, is_dst=True)
	#local_dt=local_dt.astimezone(TRUENORTH_TIMEZONE)
	return local_dt.isoformat() + 'Z'

def add_duration_to_datetime(idate,itime,duration,durationType):
	if durationType=='MIN':
		sdate=datetime.datetime.strptime(idate,'%Y-%m-%d')
		stime=datetime.datetime.strptime(itime,'%I:%M %p').time()
		local_dt=datetime.datetime.combine(sdate,stime)
		local_dt=local_dt+datetime.timedelta(minutes=duration)
		#local_dt=tz.localize(local_dt, is_dst=True)
		#local_dt=local_dt.astimezone(TRUENORTH_TIMEZONE)
		return local_dt.isoformat() + 'Z'
